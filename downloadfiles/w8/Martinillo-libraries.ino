#include <pitches.h>

/* Arduino tutorial - Buzzer / Piezo Speaker
   More info and circuit: http://www.ardumotive.com/how-to-use-a-buzzer-en.html
   Dev: Michalis Vasilakis // Date: 9/6/2015 // www.ardumotive.com */


const int buzzer = 9; //buzzer to arduino pin 9


void setup(){
 
  pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output

}

void loop(){
 
  tone(buzzer, NOTE_C3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_D3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_E3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_C3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_C3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_D3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_E3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_C3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);


    tone(buzzer, NOTE_E3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_F3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_G3, 1000); // Send 1KHz sound signal...
  delay(700);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_E3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_F3, 1000); // Send 1KHz sound signal...
  delay(400);        // ...for 1 sec
  noTone(buzzer);
  delay(50);
  tone(buzzer, NOTE_G3, 1000); // Send 1KHz sound signal...
  delay(700);        // ...for 1 sec
  
 
  noTone(buzzer);     // Stop sound...
  delay(1000);        // ...for 1sec
  
}
