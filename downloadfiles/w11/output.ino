#include <Servo.h>

/* Use a photoresistor (or photocell) to turn on an LED in the dark
   More info and circuit schematic: http://www.ardumotive.com/how-to-use-a-photoresistor-en.html
   Dev: Michalis Vasilakis // Date: 8/6/2015 // www.ardumotive.com */
   

//Constants
const int pResistor = A0; // Photoresistor at Arduino analog pin A0
Servo myservo;
int servoPos = 0;
char servoMov = 0;
int sensorValue = 0;
int sensorMin = 1023;
int sensorMax = 0;


void setup(){
  Serial.begin(9600);
  myservo.attach(3);
 pinMode(pResistor, INPUT);// Set pResistor - A0 pin as an input (optional)
 while (millis() < 5000) {
  sensorValue = analogRead(pResistor);

  if (sensorValue > sensorMax) {
    sensorMax = sensorValue;
  }

  if (sensorValue < sensorMin) {
    sensorMin = sensorValue;
  }
 }
}

void loop(){
  sensorValue = analogRead(pResistor);
  sensorValue = map(sensorValue, sensorMin, sensorMax, 0, 255);
  sensorValue = constrain(sensorValue, 0, 255);
  
  Serial.println(sensorValue);

  if ((sensorValue >= 0) && ( sensorValue <= 100)) {                                  // in steps of 1 degree 
    myservo.write(40);              // tell servo to go to position in variable 'pos' 
    delay(500);                   // waits 500 for the servo to reach the position 
 
    }

    if ((sensorValue >= 101) && ( sensorValue <= 255)) {
    for(servoPos = 20; servoPos >= 0; servoPos -= 5) // goes from 80 degrees to 0 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(servoPos);         // tell servo to go to position in variable 'pos' 
    delay(500);                   // waits 500 for the servo to reach the position
      
    }
  

  delay(500); //Small delay
}
}
