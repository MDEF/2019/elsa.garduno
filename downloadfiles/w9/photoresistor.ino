/* Use a photoresistor (or photocell) to turn on an LED in the dark
   More info and circuit schematic: http://www.ardumotive.com/how-to-use-a-photoresistor-en.html
   Dev: Michalis Vasilakis // Date: 8/6/2015 // www.ardumotive.com */
   

//Constants
const int pResistor = A0; // Photoresistor at Arduino analog pin A0
const int ledPin=9;       // Led pin at Arduino pin 9

//Variables

int sensorValue = 0;
int sensorMin = 1023;
int sensorMax = 0;


void setup(){
  Serial.begin(9600);
 pinMode(ledPin, OUTPUT);  // Set lepPin - 9 pin as an output
 pinMode(pResistor, INPUT);// Set pResistor - A0 pin as an input (optional)
 while (millis() < 5000) {
  sensorValue = analogRead(pResistor);

  if (sensorValue > sensorMax) {
    sensorMax = sensorValue;
  }

  if (sensorValue < sensorMin) {
    sensorMin = sensorValue;
  }
 }
}

void loop(){
  sensorValue = analogRead(pResistor);
  sensorValue = map(sensorValue, sensorMin, sensorMax, 0, 255);
  sensorValue = constrain(sensorValue, 0, 255);
  
  Serial.println(sensorValue);
  
  //You can change value "25"
  if (sensorValue > 25){
    digitalWrite(ledPin, LOW);  //Turn led off
  }
  else{
    digitalWrite(ledPin, HIGH); //Turn led on
  }

  delay(500); //Small delay
}
